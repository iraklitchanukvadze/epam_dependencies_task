# epam_dependencies_task


## This is repository for epam-task

### details of task
 - Create a package.json file
 - Install TypeScript as a dev dependency
 - Install rxjs library as a main dependency
 - Install json-server as a global package
 - Create a db.json file and add  content to it
 - add the following command to the scripts section of the package json "start": "json-server --watch db.json"
